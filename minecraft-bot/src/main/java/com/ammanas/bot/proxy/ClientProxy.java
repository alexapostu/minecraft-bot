package com.ammanas.bot.proxy;

import com.ammanas.bot.ClientTickSubscriber;

import net.minecraftforge.common.MinecraftForge;

public class ClientProxy implements CommonProxy {

	@Override
	public void init() {
		MinecraftForge.EVENT_BUS.register(new ClientTickSubscriber());
	}
}
