package com.ammanas.bot.jobs;

import com.ammanas.bot.util.GeneralUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.util.math.Vec3d;

public class WalkerJob implements Job {

	private static final double TELEPORTING_THRESHOLD = .1;
	private static final double WALKING_VELOCITY = .2;

	private Vec3d destination;

	public WalkerJob(Vec3d destination) {
		this.destination = destination;
	}

	public void run() {
		EntityPlayerSP player = Minecraft.getMinecraft().player;
		Vec3d crtPos = player.getPositionVector();

		Vec3d[] posAndVelocity = getVelocityAndPositionVariation(player);

		player.posX += posAndVelocity[0].xCoord;
		player.posY += posAndVelocity[0].yCoord;
		player.posZ += posAndVelocity[0].zCoord;

		player.setPositionAndUpdate(player.posX, player.posY, player.posZ);

		player.motionX = posAndVelocity[1].xCoord;
		player.motionY = posAndVelocity[1].yCoord;
		player.motionZ = posAndVelocity[1].zCoord;
		
		//GeneralUtil.lookAt(destination.xCoord, destination.yCoord, destination.zCoord, player);
	}

	private Vec3d[] getVelocityAndPositionVariation(EntityPlayerSP player) {

		double[] xVeloAndPos = getVelocityAndPositionVariationByCoord(player.posX, destination.xCoord);
		double[] yVeloAndPos = getVelocityAndPositionVariationByCoord(player.posY, destination.yCoord);
		double[] zVeloAndPos = getVelocityAndPositionVariationByCoord(player.posZ, destination.zCoord);

		Vec3d positionOffset = new Vec3d(xVeloAndPos[0], yVeloAndPos[0], zVeloAndPos[0]);
		Vec3d velocityOffset = new Vec3d(xVeloAndPos[1], yVeloAndPos[1], zVeloAndPos[1]);
		

		return new Vec3d[]{positionOffset, velocityOffset};
	}

	private double[] getVelocityAndPositionVariationByCoord(double crtPos, double desiredPos) {
		double[] velocityAndPos = new double[]{0, 0};
		if (Math.abs(crtPos - desiredPos) > TELEPORTING_THRESHOLD) {
			velocityAndPos[1] = -1 * Math.signum(crtPos - desiredPos) * WALKING_VELOCITY;
		} else {
			velocityAndPos[0] = desiredPos - crtPos;
			velocityAndPos[1] = 0;
		}
		return velocityAndPos;
	}

	@Override
	public void checkFinished() {
		EntityPlayerSP player = Minecraft.getMinecraft().player;
		Vec3d crtPos = player.getPositionVector();
		if (crtPos.equals(destination)) {
			JobManager.removeJob(this);
		}
	}
}
