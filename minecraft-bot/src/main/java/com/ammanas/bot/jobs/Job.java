package com.ammanas.bot.jobs;

public interface Job {
	public void run();
	public void checkFinished();
}
