package com.ammanas.bot.command;

import net.minecraft.util.math.Vec3d;

public class CommandUtil {


    public static Vec3d extractVectorFromCommandParams(String[] params) {
        try {
            return new Vec3d(Double.valueOf(params[0]),
                    Double.valueOf(params[1]), Double.valueOf(params[2]));
        } catch (Exception exc) {
            System.err.println(exc);
        }
        return null;
    }
}
