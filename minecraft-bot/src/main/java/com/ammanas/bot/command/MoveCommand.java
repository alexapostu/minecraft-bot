package com.ammanas.bot.command;

import com.ammanas.bot.jobs.JobManager;
import com.ammanas.bot.jobs.WalkerJob;
import com.ammanas.bot.util.ClientSidedChat;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.Vec3d;

public class MoveCommand {

	public static void execute(String[] params) {
		if (params.length != 3) {
			ClientSidedChat
					.printBotMessage("Invalid number of parameters!\n Command usage:\n "
							+ "`move xOffset yOffset zOffset`.\n Example: move 5 0 3");
		} else {
			// Command params represent the offset the player should try to move
			// in relation to it's current position
			Vec3d offset = CommandUtil.extractVectorFromCommandParams(params);
			if (offset == null) {
				ClientSidedChat.printBotMessage(
						"Invalid parameters!\n Parameters need to be of type double");
				return;
			}
			// Get the desired position based on crt position + offset
			Vec3d crtPosition = Minecraft.getMinecraft().player
					.getPositionVector();
			Vec3d desiredPos = crtPosition.add(offset);
			JobManager.addJob(new WalkerJob(desiredPos));
		}
	}
}
