package com.ammanas.bot.command;

import com.ammanas.bot.PlayerState;
import com.ammanas.bot.util.ClientSidedChat;

public class BreakCommand {
    private static boolean leftClickActive = false;

    public static boolean isLeftClickActive() {
        return leftClickActive;
    }

    public static void execute(String[] params) {
        try {
            if (params.length < 1) {
                ClientSidedChat.printBotMessage("Action requires at least 1 parameter.");
            } else {
                PlayerState.setLeftClickDown(params[0].equals("on"));
            }
        } catch (Exception exc) {
            System.err.println(exc);
            ClientSidedChat.printBotMessage("Unable to place item!");
        }

    }

}
