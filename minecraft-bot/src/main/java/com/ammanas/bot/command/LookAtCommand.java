package com.ammanas.bot.command;

import com.ammanas.bot.util.ClientSidedChat;
import com.ammanas.bot.util.GeneralUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.Vec3d;

public class LookAtCommand {

    public static void execute(String[] params) {
        if (params.length != 3) {
            ClientSidedChat.printBotMessage("Invalid number of parameters!\n Command usage:\n "
                    + "`lookat xOffset yOffset zOffset`.\n Example: lookat -200 100 30");
        } else {
            Vec3d offset = CommandUtil.extractVectorFromCommandParams(params);
            if (offset == null) {
                ClientSidedChat.printBotMessage("Invalid parameters!\n Parameters need to be of type double");
                return;
            }
            GeneralUtil.lookAt(offset.xCoord, offset.yCoord, offset.zCoord, Minecraft.getMinecraft().player);
        }

    }

}
