package com.ammanas.bot.command;

import java.util.ArrayList;
import java.util.List;

import com.ammanas.bot.jobs.JobManager;
import com.ammanas.bot.jobs.WalkerJob;
import com.ammanas.bot.util.ClientSidedChat;
import com.ammanas.bot.util.ConfigurablePathFinder;
import com.ammanas.bot.util.CorrectedWalkNodeProcessor;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathFinder;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GotoCommand {

	private static final int NR_OF_PATHFIND_ATTEMPTS = 10;

	public static void execute(String[] params) {
		if (params.length != 3) {
			ClientSidedChat.printBotMessage("Invalid number of parameters!\n Command usage:\n "
					+ "`goto xCoord yCoord zCoord`.\n Example: goto 200 63 100");
		} else {
			EntityPlayerSP player = Minecraft.getMinecraft().player;
			BlockPos newPos = extractDesiredPositionFromParams(params);
			if (newPos == null) {
				ClientSidedChat.printBotMessage("Invalid parameters! Parameters need to be of type double!");
				return;
			}

			ConfigurablePathFinder pathFinder = new ConfigurablePathFinder(new CorrectedWalkNodeProcessor());
			// Because of some rather odd inheritance strategy in the original
			// MC code, EntityPlayerSP is not an EntityLiving
			// We create an EntityZombie which is an EntityLiving using the
			// parameters of the EntityPlayerSP
			EntityZombie ez = new EntityZombie(Minecraft.getMinecraft().world);
			ez.setPosition(player.posX, player.posY, player.posZ);

			List<Path> pathsToObjective = new ArrayList<Path>();
			for (int i = 0; i < NR_OF_PATHFIND_ATTEMPTS; i++) {
				Path pathToObjective = pathFinder.findPath(Minecraft.getMinecraft().world, ez, newPos, 150000f);
				if (pathToObjective == null || pathToObjective.getCurrentPathLength() < 1) {
					if (i == 0) {
						ClientSidedChat.printBotMessage("Unable to find path!");
					}
					continue;
				}
				pathsToObjective.add(pathToObjective);
				BlockPos lastPos = new BlockPos(pathToObjective.getFinalPathPoint().xCoord,
						pathToObjective.getFinalPathPoint().yCoord, pathToObjective.getFinalPathPoint().zCoord);
				if (lastPos.equals(newPos)) {
					break;
				}
				ez.setPosition(lastPos.getX(), lastPos.getY(), lastPos.getZ());
			}
			createBotJobsFromPathPoints(pathsToObjective);
		}
	}

	private static void createBotJobsFromPathPoints(List<Path> pathsToObjective) {
		for (Path pathToObjective : pathsToObjective) {
			for (int i = 0; i < pathToObjective.getCurrentPathLength(); i++) {
				PathPoint nextPoint = pathToObjective.getPathPointFromIndex(i);
				/*
				 * Minecraft.getMinecraft().world.setBlockState(new
				 * BlockPos(nextPoint.xCoord, nextPoint.yCoord,
				 * nextPoint.zCoord), Blocks.REDSTONE_BLOCK.getDefaultState());
				 */
				Vec3d desiredPos = new Vec3d(nextPoint.xCoord + .5, nextPoint.yCoord, nextPoint.zCoord + .5);
				JobManager.addJob(new WalkerJob(desiredPos));
			}
		}
	}

	private static BlockPos extractDesiredPositionFromParams(String[] params) {
		try {
			return new BlockPos(Double.valueOf(params[0]), Double.valueOf(params[1]), Double.valueOf(params[2]));
		} catch (Exception exc) {
			System.err.println(exc);
		}
		return null;
	}

}
