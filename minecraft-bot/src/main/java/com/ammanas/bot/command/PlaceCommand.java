package com.ammanas.bot.command;

import java.lang.reflect.Method;

import com.ammanas.bot.util.ClientSidedChat;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class PlaceCommand {

	public static void execute(String[] params) {

		EntityPlayerSP player = Minecraft.getMinecraft().player;
		ItemStack heldItem = player.getHeldItemMainhand();
		ItemBlock placedItem = new ItemBlock(Block.getBlockFromItem(heldItem.getItem()));
		
		try {
			Method method = Minecraft.getMinecraft().getClass().getDeclaredMethod("rightClickMouse");
			method.setAccessible(true);
			Object r = method.invoke(Minecraft.getMinecraft());
		} catch (Exception exc) {
			System.err.println(exc);
			ClientSidedChat.printBotMessage("Unable to place item!");
		}
	}
}
