package com.ammanas.bot;

public class Reference {
	public static final String MOD_ID = "bot";
	public static final String NAME = "Lexy's bot";
	public static final String VERSION = "1.0";

	public static final String CLIENT_PROXY_CLASS = "com.ammanas.bot.proxy.ClientProxy";
	public static final String SERVER_PROXY_CLASS = "com.ammanas.bot.proxy.ServerProxy";
}
