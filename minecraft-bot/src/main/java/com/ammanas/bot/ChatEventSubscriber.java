package com.ammanas.bot;

import com.ammanas.bot.command.CommandProcessor;
import com.ammanas.bot.util.ClientSidedChat;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.ClientChatEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber
public class ChatEventSubscriber {
	private static final String BOT_COMMAND_PREFIX = "/b";
	private static final String BOT_RESPONSE_PREFIX = "[BOT]";
	private static final String COMMAND_SEPARATOR_CHARACTER = " ";

	@SubscribeEvent
	public static void interceptMessage(ClientChatEvent event) {
		if (event.getMessage().trim().startsWith(BOT_COMMAND_PREFIX)) {
			// Don't send bot commands to the server
			event.setCanceled(true);
			ClientSidedChat.printPlayerMessage(event.getMessage());
			Minecraft.getMinecraft().ingameGUI.getChatGUI().addToSentMessages(event.getMessage());
			String[] command = event.getMessage().trim().split(COMMAND_SEPARATOR_CHARACTER);
			if (command.length < 2) {
				ClientSidedChat.printBotMessage("Missing mandatory parameter `action type`");
				return;
			}

			CommandProcessor.process(command);
		} else if (event.getMessage().trim().startsWith(BOT_RESPONSE_PREFIX)) {
			event.setCanceled(true);
		}
	}
}
