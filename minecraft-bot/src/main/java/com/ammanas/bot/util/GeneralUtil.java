package com.ammanas.bot.util;

import net.minecraft.entity.player.EntityPlayer;

public class GeneralUtil {
    public static void lookAt(double px, double py, double pz, EntityPlayer me) {
        double dirx = me.getPositionEyes(1F).xCoord - px;
        double diry = me.getPositionEyes(1F).yCoord - py;
        double dirz = me.getPositionEyes(1F).zCoord - pz;

        double len = Math.sqrt(dirx * dirx + diry * diry + dirz * dirz);

        dirx /= len;
        diry /= len;
        dirz /= len;

        double pitch = Math.asin(diry);
        double yaw = Math.atan2(dirz, dirx);

        // to degree
        pitch = pitch * 180.0 / Math.PI;
        yaw = yaw * 180.0 / Math.PI;

        yaw += 90f;
        me.rotationPitch = (float) pitch;
        me.rotationYaw = (float) yaw;
    }

    public static void trySleep(int sleepTimeInMillis) {
        try {
            Thread.sleep(sleepTimeInMillis);
        } catch (Exception exc) {
            System.err.println("Could not sleep!" + exc.toString());
        }

    }
}
