package com.ammanas.bot;

public class PlayerState {
    private static boolean leftClickDown = false;

    public static boolean isLeftClickDown() {
        return leftClickDown;
    }

    public static void setLeftClickDown(boolean clickDown) {
        leftClickDown = clickDown;
    }
}
