package com.ammanas.bot.jobs;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.util.math.Vec3d;

public class WalkerJob implements Job {

	private static final double TELEPORTING_THRESHOLD = .1;
	private static final double WALKING_VELOCITY = .2;

	private Vec3d destination;

	public WalkerJob(Vec3d destination) {
		this.destination = destination;
	}

	public void run() {
		EntityPlayerSP player = Minecraft.getMinecraft().player;
		Vec3d crtPos = player.getPositionVector();
		if (Math.abs(crtPos.xCoord - destination.xCoord) > TELEPORTING_THRESHOLD) {
			player.setVelocity(-1 * Math.signum(crtPos.xCoord - destination.xCoord) * WALKING_VELOCITY, 0, 0);

		} else {
			player.setPosition(destination.xCoord, crtPos.yCoord, crtPos.zCoord);
			player.motionX = 0;
		}
	}

	private Vec3d[] getVelocityAndPositionVariation() {
		Vec3d positionOffset = new Vec3d(0, 0, 0);
		Vec3d velocityOffset = new Vec3d(0, 0, 0);

		return new Vec3d[]{positionOffset, velocityOffset};
	}

	private double[] getVelocityAndPositionVariationByCoord(double crtPos, double desiredPos) {
		double[] velocityAndPos = new double[]{0, 0};
		if (Math.abs(crtPos - desiredPos) > TELEPORTING_THRESHOLD) {
			velocityAndPos[1] = -1 * Math.signum(crtPos - desiredPos) * WALKING_VELOCITY;
		} else {
			velocityAndPos[1] = 0;
		}
		return velocityAndPos;
	}

	@Override
	public void checkFinished() {
		EntityPlayerSP player = Minecraft.getMinecraft().player;
		Vec3d crtPos = player.getPositionVector();
		if (crtPos.xCoord == destination.xCoord) {
			JobManager.removeJob(this);
		}
	}
}
