package com.ammanas.bot.util;

import java.lang.reflect.Field;
import java.util.Set;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.pathfinding.NodeProcessor;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathFinder;
import net.minecraft.pathfinding.PathHeap;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class ConfigurablePathFinder extends PathFinder {

	public ConfigurablePathFinder(NodeProcessor processor) {
		super(processor);
	}

	public PathHeap getPath() {
		return (PathHeap) getPrivateSuperclassField("path");
	}

	public Set<PathPoint> getClosedSet() {
		return (Set<PathPoint>) getPrivateSuperclassField("closedSet");
	}

	public PathPoint[] getPathOptions() {
		return (PathPoint[]) getPrivateSuperclassField("pathOptions");
	}

	public NodeProcessor getNodeProcessor() {
		return (NodeProcessor) getPrivateSuperclassField("nodeProcessor");
	}

	private Object getPrivateSuperclassField(String fieldName) {
		try {
			Class<?> clazz = getClass().getSuperclass();
			Field field = clazz.getDeclaredField("closedSet");
			field.setAccessible(true);
			return field.get(this);
		} catch (Exception exc) {
			System.err.println(exc);
		}
		return null;
	}

	@Nullable
	public Path findPath(IBlockAccess worldIn, EntityLiving p_186333_2_, Entity p_186333_3_, float p_186333_4_) {
		return this.findPath(worldIn, p_186333_2_, p_186333_3_.posX, p_186333_3_.getEntityBoundingBox().minY,
				p_186333_3_.posZ, p_186333_4_);
	}

	@Nullable
	public Path findPath(IBlockAccess worldIn, EntityLiving p_186336_2_, BlockPos p_186336_3_, float p_186336_4_) {
		return this.findPath(worldIn, p_186336_2_, (double) ((float) p_186336_3_.getX() + 0.5F),
				(double) ((float) p_186336_3_.getY() + 0.5F), (double) ((float) p_186336_3_.getZ() + 0.5F),
				p_186336_4_);
	}

	@Nullable
	private Path findPath(IBlockAccess worldIn, EntityLiving p_186334_2_, double p_186334_3_, double p_186334_5_,
			double p_186334_7_, float p_186334_9_) {
		this.getPath().clearPath();
		this.getNodeProcessor().initProcessor(worldIn, p_186334_2_);
		PathPoint pathpoint = this.getNodeProcessor().getStart();
		PathPoint pathpoint1 = this.getNodeProcessor().getPathPointToCoords(p_186334_3_, p_186334_5_, p_186334_7_);
		Path path = this.findPath(pathpoint, pathpoint1, p_186334_9_);
		this.getNodeProcessor().postProcess();
		return path;
	}

	@Nullable
	private Path findPath(PathPoint p_186335_1_, PathPoint p_186335_2_, float p_186335_3_) {
		p_186335_1_.totalPathDistance = 0.0F;
		p_186335_1_.distanceToNext = p_186335_1_.distanceManhattan(p_186335_2_);
		p_186335_1_.distanceToTarget = p_186335_1_.distanceToNext;
		this.getPath().clearPath();
		this.getClosedSet().clear();
		this.getPath().addPoint(p_186335_1_);
		PathPoint pathpoint = p_186335_1_;
		int i = 0;

		while (!this.getPath().isPathEmpty()) {
			++i;

			if (i >= 200) {
				break;
			}

			PathPoint pathpoint1 = this.getPath().dequeue();

			if (pathpoint1.equals(p_186335_2_)) {
				pathpoint = p_186335_2_;
				break;
			}

			if (pathpoint1.distanceManhattan(p_186335_2_) < pathpoint.distanceManhattan(p_186335_2_)) {
				pathpoint = pathpoint1;
			}

			pathpoint1.visited = true;
			int j = this.getNodeProcessor().findPathOptions(this.getPathOptions(), pathpoint1, p_186335_2_,
					p_186335_3_);

			for (int k = 0; k < j; ++k) {
				PathPoint pathpoint2 = this.getPathOptions()[k];
				float f = pathpoint1.distanceManhattan(pathpoint2);
				pathpoint2.distanceFromOrigin = pathpoint1.distanceFromOrigin + f;
				pathpoint2.cost = f + pathpoint2.costMalus;
				float f1 = pathpoint1.totalPathDistance + pathpoint2.cost;

				if (pathpoint2.distanceFromOrigin < p_186335_3_
						&& (!pathpoint2.isAssigned() || f1 < pathpoint2.totalPathDistance)) {
					pathpoint2.previous = pathpoint1;
					pathpoint2.totalPathDistance = f1;
					pathpoint2.distanceToNext = pathpoint2.distanceManhattan(p_186335_2_) + pathpoint2.costMalus;

					if (pathpoint2.isAssigned()) {
						this.getPath().changeDistance(pathpoint2,
								pathpoint2.totalPathDistance + pathpoint2.distanceToNext);
					} else {
						pathpoint2.distanceToTarget = pathpoint2.totalPathDistance + pathpoint2.distanceToNext;
						this.getPath().addPoint(pathpoint2);
					}
				}
			}
		}

		if (pathpoint == p_186335_1_) {
			return null;
		} else {
			Path path = this.createEntityPath(p_186335_1_, pathpoint);
			return path;
		}
	}
	/**
	 * Returns a new PathEntity for a given start and end point
	 */
	private Path createEntityPath(PathPoint start, PathPoint end) {
		int i = 1;

		for (PathPoint pathpoint = end; pathpoint.previous != null; pathpoint = pathpoint.previous) {
			++i;
		}

		PathPoint[] apathpoint = new PathPoint[i];
		PathPoint pathpoint1 = end;
		--i;

		for (apathpoint[i] = end; pathpoint1.previous != null; apathpoint[i] = pathpoint1) {
			pathpoint1 = pathpoint1.previous;
			--i;
		}

		return new Path(apathpoint);
	}

}
